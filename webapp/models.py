from django.db import models
from django.conf import settings



class Employee(models.Model):
    firstname = models.CharField(max_length=200, null=True, blank=True, verbose_name='Name')
    secondname = models.CharField(max_length=200, null=True, blank=True, verbose_name='Surname')
    email = models.EmailField(max_length=200, null=False, blank=False, verbose_name='Email')
    password = models.CharField(max_length=200, null=False, blank=False, verbose_name='Password')
    info = models.CharField(max_length=200, null=True, blank=True, verbose_name='Information')
    gender = models.CharField(choices=settings.GENDER_CHOICES, max_length=128, default='M')
    city = models.CharField(choices=settings.CITY_CHOICES, max_length=128, default='Bishkek')
    is_active = models.BooleanField(default=True)

