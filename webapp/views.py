from django.shortcuts import render, get_object_or_404, redirect
from webapp.models import Employee
from django.conf import settings

# Create your views here.


def index(request):
    employes = Employee.objects.all()
    context = {
        'employes': employes,
    }
    return render(request, 'index.html', context)


def view_employee(request, employee_pk):
    employee = get_object_or_404(Employee, pk=employee_pk)
    cities = settings.CITY_CHOICES
    gender = settings.GENDER_CHOICES
    for c in cities:
        if c[0] == employee.city:
            employee.city = c[1]
            break

    for g in gender:
        if g[0] == employee.gender:
            employee.gender = g[1]
            break

    context = {
        'employee': employee,
    }
    return render(request, 'view.html', context)


def update_employee(request, employee_pk):
    employee = get_object_or_404(Employee, pk=employee_pk)

    if request.method == 'GET':
        context = {
            'employee': employee,
            'cities': settings.CITY_CHOICES
        }
        return render(request, 'update.html', context)

    elif request.method == 'POST':
        employee.firstname = request.POST.get('firstname')
        employee.secondname = request.POST.get('secondname')
        employee.email = request.POST.get('email')
        employee.password = request.POST.get('password')
        employee.info = request.POST.get('info')
        employee.gender = request.POST.get('gender')
        employee.city = request.POST.get('city')
        if request.POST.get('is_active') == 'on':
            employee.is_active = True
        else:
            employee.is_active = False
        employee.save()
        return redirect('view_employee', employee_pk=employee.pk)


def create_employee(request):
    if request.method == 'GET':
        context = {
            'cities': settings.CITY_CHOICES
        }
        return render(request, 'create.html', context)

    elif request.method == 'POST':
        firstname = request.POST.get('firstname')
        secondname = request.POST.get('secondname')
        email = request.POST.get('email')
        password = request.POST.get('password')
        info = request.POST.get('info')
        gender = request.POST.get('gender')
        city = request.POST.get('city')
        if request.POST.get('is_active') == 'on':
            is_active = True
        else:
            is_active = False
        employee = Employee.objects.create(firstname=firstname, secondname=secondname, email=email, password=password,
                                           info=info, gender=gender, city=city, is_active=is_active)
        return redirect('view_employee', employee_pk=employee.pk)


def delete_employee(request, employee_pk):
    employee = get_object_or_404(Employee, pk=employee_pk)
    employee.delete()
    return redirect('index')